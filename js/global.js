$(document).ready(function() {
	displayVideoAsPopup(".video-play-btn","href");
	iosVideoFix();
})


function displayVideoAsPopup(selector,attr){
	$(selector).each(function(i,e){
		var videoSrc=$(e).attr(attr);
		if($(e)[0].localName=="a"){
			$(e).attr("href","javascript:void(0)");
		}
		$(e).magnificPopup({
			items: {
				type: 'inline',
				src: '<video controls preload="auto" width="100%" height="100%" src="'+videoSrc+'"></video>',
			},
			callbacks: {
				open: function() {
					$(this.content)[0].play()
				},
				close: function() {
					$(this.content)[0].load()
				}
			}
		});
	});
}

function iosVideoFix() {
	$('video').each(function(i) {
		if ($(this).attr('autoplay')){
			$(this)[i].load();
			$(this)[i].play();
	    } else {
			$(this)[i].load();
	    }
	})
}


