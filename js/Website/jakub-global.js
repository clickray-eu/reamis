$(function () {
    $('.header .menu-links .widget-type-menu .hs_cos_wrapper_type_menu > .hs-menu-wrapper > ul').slicknav({
        label: '',
        init: function init() {
            $('.slicknav_menu').prepend('<a class="slick-logo" href="index.php"><img class="logo" src="//cdn2.hubspot.net/hubfs/469189/CR%20TEMPLATES/IMG/logo/logo-reamis-red.png" alt="Reamis logo red" /></a>');
            $('.slicknav_menu .slicknav_btn .slicknav_icon').html('<i class="fa fa-bars" aria-hidden="true"></i>');
        },
        beforeOpen: function beforeOpen(event) {
            if (event.hasClass('slicknav_btn')) {
                $('.slicknav_menu .slicknav_btn .slicknav_icon').html('<i class="fa fa-times-circle" aria-hidden="true"></i>');
            }
        },
        afterOpen: function afterOpen(event) {
            if (event.hasClass('slicknav_btn')) {
                $("window, body").css("overflow-y", "hidden");
            }
        },
        afterClose: function afterClose(event) {
            if (event.hasClass('slicknav_btn')) {
                $('.slicknav_menu .slicknav_btn .slicknav_icon').html('<i class="fa fa-bars" aria-hidden="true"></i>');
                $("window, body").css("overflow-y", "auto");
            }
        }
    });
});
if (navigator.userAgent.search("Windows NT 10.0") == -1) {
    $(function () {
        var $window = $(window);
        var scrollTime = 1.2;
        var scrollDistance = 150;

        $window.on("mousewheel DOMMouseScroll", function (event) {

            event.preventDefault();

            var delta = event.originalEvent.wheelDelta / 120 || -event.originalEvent.detail / 3;
            var scrollTop = $window.scrollTop();
            var finalScroll = scrollTop - parseInt(delta * scrollDistance);

            TweenMax.to($window, scrollTime, {
                scrollTo: { y: finalScroll, autoKill: true },
                ease: Power1.easeOut,
                overwrite: 5
            });
        });
    });
}

$(document).ready(function () {
    var scrollTop = 0;
    var logo = $('header.header .widget-type-menu .hs-menu-wrapper > ul, header.header .widget-type-logo');
    var logo_height = parseInt(logo.css('height'));
    console.log(logo_height);
    $(window).scroll(function (event) {
        scrollTop = $(this).scrollTop();
        var temp = 86 - scrollTop;

        if (scrollTop < 41) {
            // add/remove class active
            if ($('header.header').hasClass('active')) {
                $('header.header').removeClass('active');
            }

            // Menu resize
            $('header.header .widget-type-logo').css('height', temp + 'px');
            $('header.header .widget-type-logo .sub-logo a img').css('max-height', temp + 'px');
            $('header.header .hs-menu-depth-1 > .hs-menu-children-wrapper').css('top', temp - 2 + 'px');
            $('header.header .widget-type-menu .hs-menu-wrapper > ul').css('height', temp + 'px');
            $('header.header .menu-links .widget-type-menu .hs-menu-wrapper > ul > li').not(':last-child').find('> a').css('line-height', temp + 'px');
            $('header.header .menu-language .widget-type-menu .hs-menu-wrapper > ul > li').find('> a').css('line-height', temp + 'px');


            // Landings
            if ($('body').hasClass('landing')) {
                if($('body').width() <= 992) {
                    $('.sub-logo-red').css('opacity', '1');
                    $('.sub-logo-white').css('opacity', '0');
                }
                else {
                    $('.sub-logo-red').css('opacity', '0');
                    $('.sub-logo-white').css('opacity', '1');
                }
            }
        } else {
            // add/remove class active
            if (!$('header.header').hasClass('active')) {
                $('header.header').addClass('active');
            }

            // Menu resize
            $('header.header .widget-type-logo').css('height', '45px');
            $('header.header .widget-type-logo .sub-logo a img').css('max-height', '45px');
            $('header.header .hs-menu-depth-1 > .hs-menu-children-wrapper').css('top', '43px');
            $('header.header .widget-type-menu .hs-menu-wrapper > ul').css('height', '45px');
            $('header.header .menu-language .widget-type-menu .hs-menu-wrapper > ul > li').find('> a').css('line-height', '45px');
            $('header.header .menu-links .widget-type-menu .hs-menu-wrapper > ul > li').not(':last-child').find('> a').css('line-height', '45px');

            // Landings
            if ($('body').hasClass('landing')) {
                if($('body').width() <= 992) {
                    $('.sub-logo-red').css('opacity', '1');
                    $('.sub-logo-white').css('opacity', '0');
                }
                else {
                    $('.sub-logo-red').css('opacity', '1');
                    $('.sub-logo-white').css('opacity', '0');
                }
            }
        }

        // if($('body').hasClass('menu-from-start')) {
        //     if (scrollTop < 50) {
        //     }
        //     if(scrollTop > 1) {
        //         if (!$('header.header').hasClass('active')) {
        //             $('header.header').addClass('active');
        //         }
        //     }
        //     else {
        //         if ($('header.header').hasClass('active')) {
        //             $('header.header').removeClass('active');
        //         }
        //     }
        // }
        // else {

        // }
    });
});