$(window).load(function() {    
    var prefsForm = $('.preferences-section form.hs-form');
    var globalUnsub = $('input[id^="global_unsubscribe"]');
    var onEvent = "";
   if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
    	onEvent = 'click';
    } else {
    	onEvent = 'change';
    }

    if (globalUnsub.attr('checked')) {
        globalUnsub.click().removeAttr('checked')
    } else {
        globalUnsub.removeAttr('checked')
    }
    
    prefsForm.find('input[type="checkbox"]').not(globalUnsub).not('input[value="disabled"]').on(onEvent, function() {
        var count = 0;
        var inputList = $(this).parents('.inputs-list');

        inputList.find('input[type="checkbox"]').not('input[value="disabled"]').each(function(i, e) {
            if ($(e).is(':checked')) {
                count++;
            }
        });
        
        if (count == 0) {
            if (inputList.find('input[value="disabled"]').is(':checked')) {
                inputList.find('input[value="disabled"]').attr('checked', true);
            } else {
                inputList.find('input[value="disabled"]').click().attr('checked', true);
            }
        } else {
            if (inputList.find('input[value="disabled"]').is(':checked')) {
                inputList.find('input[value="disabled"]').click().removeAttr('checked');
            } else {
                inputList.find('input[value="disabled"]').removeAttr('checked');
            }
        }
    })


    // Global unsubsribe handle
	globalUnsub.on('click', function() {
        if ($(this).attr('checked')) {
            $(this).removeAttr('checked');
            prefsForm.find('.hs-form-field').not('.hs_global_unsubscribe').each(function(i, e) {
            	$(e).removeClass('disabled');
            });
            
            // uncheck all disabled
            prefsForm.find('input[value="disabled"]').each(function() {
                $(this).click().removeAttr('checked', true);
            });
            
            // enable all other checkboxes
            prefsForm.find('input[type="checkbox"]').not(this).not('input[value="disabled"]').each(function() {
                $(this).removeAttr('disabled');
            });
        } else {
            $(this).attr('checked', true);
            prefsForm.find('.hs-form-field').not('.hs_global_unsubscribe').each(function(i, e) {
            	$(e).addClass('disabled');
            });

            // check all disabled
            prefsForm.find('input[value="disabled"]').each(function() {
                if ($(this).is(':checked') == false) {
                    $(this).click().attr('checked', true);
                }
            });
            
            // disable all other checkboxes
            prefsForm.find('input[type="checkbox"]').not(this).not('input[value="disabled"]').each(function() {
                $(this).attr('disabled', true);
            });
        }

	});
});