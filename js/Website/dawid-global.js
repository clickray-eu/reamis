$(document).ready(function () {

    //Scroll up in footer
    footerScrollUp();

});

$( window ).scroll(function() {

 	//Scroll up fade
 	footerScrollUpFade();
});


function footerScrollUp() {
    $(".scroll-top").click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 500);
        return false;
    });
}

function footerScrollUpFade(){

   var windowHeight = $(window).scrollTop();
   var scrollTop = $('.scroll-top');

	 	if(windowHeight > 500)
	 	{

	 		scrollTop.css("opacity", "0.7");

	 	}
	 	else
	 	{
	 		scrollTop.css("opacity", "0");
	 	}
}
