function waitForLoad(wrapper, element, callback) {
   if ($(wrapper).length > 0) {
       $(wrapper).each(function(i, el) {
         var waitForLoad = setInterval(function() {
         if ($(el).length == $(el).find(element).length) {
            clearInterval(waitForLoad);
            callback($(el), $(el).find(element));
         }
       }, 50);
   });
   }
}

waitForLoad(".hs_cos_wrapper_type_form","form.hs-form", function(){
    if(jQuery('form select option:selected').attr("disabled")==undefined){
      jQuery('form select').css('color', 'rgb(51, 51, 51)');
    }
    $('form select').on('change', function() {
        $('form select').css('color','#333');
    });
});