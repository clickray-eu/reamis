"use strict";

// file: forms.js
function waitForLoad(wrapper, element, callback) {
    if ($(wrapper).length > 0) {
        $(wrapper).each(function (i, el) {
            var waitForLoad = setInterval(function () {
                if ($(el).length == $(el).find(element).length) {
                    clearInterval(waitForLoad);
                    callback($(el), $(el).find(element));
                }
            }, 50);
        });
    }
}

waitForLoad(".hs_cos_wrapper_type_form", "form.hs-form", function () {
    if (jQuery('form select option:selected').attr("disabled") == undefined) {
        jQuery('form select').css('color', 'rgb(51, 51, 51)');
    }
    $('form select').on('change', function () {
        $('form select').css('color', '#333');
    });
});
// end file: forms.js

// file: global.js
$(document).ready(function () {
    displayVideoAsPopup(".video-play-btn", "href");
    iosVideoFix();
});

function displayVideoAsPopup(selector, attr) {
    $(selector).each(function (i, e) {
        var videoSrc = $(e).attr(attr);
        if ($(e)[0].localName == "a") {
            $(e).attr("href", "javascript:void(0)");
        }
        $(e).magnificPopup({
            items: {
                type: 'inline',
                src: '<video controls preload="auto" width="100%" height="100%" src="' + videoSrc + '"></video>'
            },
            callbacks: {
                open: function open() {
                    $(this.content)[0].play();
                },
                close: function close() {
                    $(this.content)[0].load();
                }
            }
        });
    });
}

function iosVideoFix() {
    $('video').each(function (i) {
        if ($(this).attr('autoplay')) {
            $(this)[i].load();
            $(this)[i].play();
        } else {
            $(this)[i].load();
        }
    });
}
// end file: global.js

// file: Website/dawid-global.js
$(document).ready(function () {

    //Scroll up in footer
    footerScrollUp();
});

$(window).scroll(function () {

    //Scroll up fade
    footerScrollUpFade();
});

function footerScrollUp() {
    $(".scroll-top").click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 500);
        return false;
    });
}

function footerScrollUpFade() {

    var windowHeight = $(window).scrollTop();
    var scrollTop = $('.scroll-top');

    if (windowHeight > 500) {

        scrollTop.css("opacity", "0.7");
    } else {
        scrollTop.css("opacity", "0");
    }
}

// end file: Website/dawid-global.js

// file: Website/jakub-global.js
$(function () {
    $('.header .menu-links .widget-type-menu .hs_cos_wrapper_type_menu > .hs-menu-wrapper > ul').slicknav({
        label: '',
        init: function init() {
            $('.slicknav_menu').prepend('<a class="slick-logo" href="index.php"><img class="logo" src="//cdn2.hubspot.net/hubfs/469189/CR%20TEMPLATES/IMG/logo/logo-reamis-red.png" alt="Reamis logo red" /></a>');
            $('.slicknav_menu .slicknav_btn .slicknav_icon').html('<i class="fa fa-bars" aria-hidden="true"></i>');
        },
        beforeOpen: function beforeOpen(event) {
            if (event.hasClass('slicknav_btn')) {
                $('.slicknav_menu .slicknav_btn .slicknav_icon').html('<i class="fa fa-times-circle" aria-hidden="true"></i>');
            }
        },
        afterOpen: function afterOpen(event) {
            if (event.hasClass('slicknav_btn')) {
                $("window, body").css("overflow-y", "hidden");
            }
        },
        afterClose: function afterClose(event) {
            if (event.hasClass('slicknav_btn')) {
                $('.slicknav_menu .slicknav_btn .slicknav_icon').html('<i class="fa fa-bars" aria-hidden="true"></i>');
                $("window, body").css("overflow-y", "auto");
            }
        }
    });
});
if (navigator.userAgent.search("Windows NT 10.0") == -1) {
    $(function () {
        var $window = $(window);
        var scrollTime = 1.2;
        var scrollDistance = 150;

        $window.on("mousewheel DOMMouseScroll", function (event) {

            event.preventDefault();

            var delta = event.originalEvent.wheelDelta / 120 || -event.originalEvent.detail / 3;
            var scrollTop = $window.scrollTop();
            var finalScroll = scrollTop - parseInt(delta * scrollDistance);

            TweenMax.to($window, scrollTime, {
                scrollTo: { y: finalScroll, autoKill: true },
                ease: Power1.easeOut,
                overwrite: 5
            });
        });
    });
}

$(document).ready(function () {
    var scrollTop = 0;
    var logo = $('header.header .widget-type-menu .hs-menu-wrapper > ul, header.header .widget-type-logo');
    var logo_height = parseInt(logo.css('height'));
    console.log(logo_height);
    $(window).scroll(function (event) {
        scrollTop = $(this).scrollTop();
        var temp = 86 - scrollTop;

        if (scrollTop < 41) {
            // add/remove class active
            if ($('header.header').hasClass('active')) {
                $('header.header').removeClass('active');
            }

            // Menu resize
            $('header.header .widget-type-logo').css('height', temp + 'px');
            $('header.header .widget-type-logo .sub-logo a img').css('max-height', temp + 'px');
            $('header.header .hs-menu-depth-1 > .hs-menu-children-wrapper').css('top', temp - 2 + 'px');
            $('header.header .widget-type-menu .hs-menu-wrapper > ul').css('height', temp + 'px');
            $('header.header .menu-links .widget-type-menu .hs-menu-wrapper > ul > li').not(':last-child').find('> a').css('line-height', temp + 'px');
            $('header.header .menu-language .widget-type-menu .hs-menu-wrapper > ul > li').find('> a').css('line-height', temp + 'px');

            // Landings
            if ($('body').hasClass('landing')) {
                if ($('body').width() <= 992) {
                    $('.sub-logo-red').css('opacity', '1');
                    $('.sub-logo-white').css('opacity', '0');
                } else {
                    $('.sub-logo-red').css('opacity', '0');
                    $('.sub-logo-white').css('opacity', '1');
                }
            }
        } else {
            // add/remove class active
            if (!$('header.header').hasClass('active')) {
                $('header.header').addClass('active');
            }

            // Menu resize
            $('header.header .widget-type-logo').css('height', '45px');
            $('header.header .widget-type-logo .sub-logo a img').css('max-height', '45px');
            $('header.header .hs-menu-depth-1 > .hs-menu-children-wrapper').css('top', '43px');
            $('header.header .widget-type-menu .hs-menu-wrapper > ul').css('height', '45px');
            $('header.header .menu-language .widget-type-menu .hs-menu-wrapper > ul > li').find('> a').css('line-height', '45px');
            $('header.header .menu-links .widget-type-menu .hs-menu-wrapper > ul > li').not(':last-child').find('> a').css('line-height', '45px');

            // Landings
            if ($('body').hasClass('landing')) {
                if ($('body').width() <= 992) {
                    $('.sub-logo-red').css('opacity', '1');
                    $('.sub-logo-white').css('opacity', '0');
                } else {
                    $('.sub-logo-red').css('opacity', '1');
                    $('.sub-logo-white').css('opacity', '0');
                }
            }
        }

        // if($('body').hasClass('menu-from-start')) {
        //     if (scrollTop < 50) {
        //     }
        //     if(scrollTop > 1) {
        //         if (!$('header.header').hasClass('active')) {
        //             $('header.header').addClass('active');
        //         }
        //     }
        //     else {
        //         if ($('header.header').hasClass('active')) {
        //             $('header.header').removeClass('active');
        //         }
        //     }
        // }
        // else {

        // }
    });
});
// end file: Website/jakub-global.js

// file: Website/preferences-page.js
$(window).load(function () {
    var prefsForm = $('.preferences-section form.hs-form');
    var globalUnsub = $('input[id^="global_unsubscribe"]');
    var onEvent = "";
    if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
        onEvent = 'click';
    } else {
        onEvent = 'change';
    }

    if (globalUnsub.attr('checked')) {
        globalUnsub.click().removeAttr('checked');
    } else {
        globalUnsub.removeAttr('checked');
    }

    prefsForm.find('input[type="checkbox"]').not(globalUnsub).not('input[value="disabled"]').on(onEvent, function () {
        var count = 0;
        var inputList = $(this).parents('.inputs-list');

        inputList.find('input[type="checkbox"]').not('input[value="disabled"]').each(function (i, e) {
            if ($(e).is(':checked')) {
                count++;
            }
        });

        if (count == 0) {
            if (inputList.find('input[value="disabled"]').is(':checked')) {
                inputList.find('input[value="disabled"]').attr('checked', true);
            } else {
                inputList.find('input[value="disabled"]').click().attr('checked', true);
            }
        } else {
            if (inputList.find('input[value="disabled"]').is(':checked')) {
                inputList.find('input[value="disabled"]').click().removeAttr('checked');
            } else {
                inputList.find('input[value="disabled"]').removeAttr('checked');
            }
        }
    });

    // Global unsubsribe handle
    globalUnsub.on('click', function () {
        if ($(this).attr('checked')) {
            $(this).removeAttr('checked');
            prefsForm.find('.hs-form-field').not('.hs_global_unsubscribe').each(function (i, e) {
                $(e).removeClass('disabled');
            });

            // uncheck all disabled
            prefsForm.find('input[value="disabled"]').each(function () {
                $(this).click().removeAttr('checked', true);
            });

            // enable all other checkboxes
            prefsForm.find('input[type="checkbox"]').not(this).not('input[value="disabled"]').each(function () {
                $(this).removeAttr('disabled');
            });
        } else {
            $(this).attr('checked', true);
            prefsForm.find('.hs-form-field').not('.hs_global_unsubscribe').each(function (i, e) {
                $(e).addClass('disabled');
            });

            // check all disabled
            prefsForm.find('input[value="disabled"]').each(function () {
                if ($(this).is(':checked') == false) {
                    $(this).click().attr('checked', true);
                }
            });

            // disable all other checkboxes
            prefsForm.find('input[type="checkbox"]').not(this).not('input[value="disabled"]').each(function () {
                $(this).attr('disabled', true);
            });
        }
    });
});
// end file: Website/preferences-page.js

// file: Modules/smoothScroll.js
$(function () {
    $('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top - 50
                }, 1000);
                return false;
            }
        }
    });
});
// end file: Modules/smoothScroll.js
//# sourceMappingURL=template.js.map
